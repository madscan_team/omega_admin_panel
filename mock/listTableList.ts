// eslint-disable-next-line import/no-extraneous-dependencies
import { Request, Response } from 'express';
import { parse } from 'url';
import { TableListItem, TableListParams } from '@/pages/ManageUsers/data';
import { TableListItem as WidgetListItem } from '@/pages/ManageWidgets/data';
import { TableListItem as FeatureListItem } from '@/pages/ManageFeatures/data';

// mock tableListDataSource
const genListForUsers = (current: number, users: any) => {
  const tableListDataSource: TableListItem[] = [];
  for (let i = 0; i < users.length; i += 1) {
    const index = (current - 1) * 10 + i;
    tableListDataSource.push({
      key: index,
      disabled: i % 6 === 0,
      // href: 'https://ant.design',
      avatar: users[i].profile,
      name: `${users[i].firstname || ""} ${users[i].lastname || ""}`,
      _id: users[i].id,
      ipAddress: users[i].clientip,
      username: users[i].username,
      about: users[i].about,
      signature: users[i].signature,
      phoneNumber: users[i].phone,
      email: users[i].email
      // owner: '曲丽丽',
      // desc: '这是一段描述',
      // callNo: Math.floor(Math.random() * 1000),
      // status: Math.floor(Math.random() * 10) % 4,
      // updatedAt: new Date(),
      // createdAt: new Date(),
      // progress: Math.ceil(Math.random() * 100),
    });
  }
  tableListDataSource.reverse();
  return tableListDataSource;
};

const genListForWidgets = (current: number, widgets: any) => {
  const tableListDataSource: WidgetListItem[] = [];

  for (let i = 0; i < widgets.length; i += 1) {
    tableListDataSource.push({
      name: widgets[i].name,
      _id: widgets[i]._id,
      subWidgets: widgets[i].subWidgets
    });
  }
  tableListDataSource.reverse();
  return tableListDataSource;
};

const genListForFeatures = (current: number, features: any) => {
  const tableListDataSource: FeatureListItem[] = [];

  for (let i = 0; i < features.length; i += 1) {
    tableListDataSource.push({
      defaultvalue: features[i].defaultvalue,
      key: features[i].key,
      id: features[i].id,
      limit: features[i].limit,
      valuetype: features[i].valuetype
    });
  }
  tableListDataSource.reverse();
  return tableListDataSource;
};

function getRule(req: Request, res: Response, u: string) {
  let realUrl = u;
  if (!realUrl || Object.prototype.toString.call(realUrl) !== '[object String]') {
    realUrl = req.url;
  }
  const { current = 1, pageSize = 10 } = req.query;
  const params = (parse(realUrl, true).query as unknown) as TableListParams;

  let tableListDataSource: any;
  if (params.users) {
    tableListDataSource = genListForUsers(1, JSON.parse(params.users));
  }
  else if (params.widgets) {
    tableListDataSource = genListForWidgets(1, JSON.parse(params.widgets));
  }
  else if (params.features) {
    tableListDataSource = genListForFeatures(1, JSON.parse(params.features));
  }
  console.log(tableListDataSource, 'qweqwe')
  let dataSource = [...tableListDataSource].slice(
    ((current as number) - 1) * (pageSize as number),
    (current as number) * (pageSize as number),
  );
  const sorter = JSON.parse(params.sorter as any);
  if (sorter) {
    dataSource = dataSource.sort((prev, next) => {
      let sortNumber = 0;
      Object.keys(sorter).forEach((key) => {
        if (sorter[key] === 'descend') {
          if (prev[key] - next[key] > 0) {
            sortNumber += -1;
          } else {
            sortNumber += 1;
          }
          return;
        }
        if (prev[key] - next[key] > 0) {
          sortNumber += 1;
        } else {
          sortNumber += -1;
        }
      });
      return sortNumber;
    });
  }
  if (params.filter) {
    const filter = JSON.parse(params.filter as any) as {
      [key: string]: string[];
    };
    if (Object.keys(filter).length > 0) {
      dataSource = dataSource.filter((item) => {
        return Object.keys(filter).some((key) => {
          if (!filter[key]) {
            return true;
          }
          if (filter[key].includes(`${item[key]}`)) {
            return true;
          }
          return false;
        });
      });
    }
  }

  if (params.name) {
    dataSource = dataSource.filter((data) => data.name.includes(params.name || ''));
  }
  const result = {
    data: dataSource,
    total: tableListDataSource.length,
    success: true,
    pageSize,
    current: parseInt(`${params.currentPage}`, 10) || 1,
  };
  return res.json(result);
}

function postRule(req: Request, res: Response, u: string, b: Request) {
  console.log('qweeeee')
  let tableListDataSource = genListForUsers(1, 11);
  let realUrl = u;
  if (!realUrl || Object.prototype.toString.call(realUrl) !== '[object String]') {
    realUrl = req.url;
  }

  const body = (b && b.body) || req.body;
  const { method, name, ipAddress, key, phoneNumber, about, signature } = body;
  console.log(key, 'a')

  switch (method) {
    /* eslint no-case-declarations:0 */
    case 'delete':
      tableListDataSource = tableListDataSource.filter((item) => key.indexOf(item.key) === -1);
      break;
    case 'post':
      (() => {
        // const i = Math.ceil(Math.random() * 10000);
        const newUser: any = { //remove any
          key: tableListDataSource.length,
          // href: 'https://ant.design',
          // avatar: [
          //   'https://gw.alipayobjects.com/zos/rmsportal/eeHMaZBwmTvLdIwMfBpg.png',
          //   'https://gw.alipayobjects.com/zos/rmsportal/udxAbMEhpwthVVcjLXik.png',
          // ][i % 2],
          name,
          ipAddress,
          phoneNumber,
          about,
          signature
        };
        tableListDataSource.unshift(newUser);
        return res.json(newUser);
      })();
      return;

    case 'update':
      (() => {
        let newUser = {};
        tableListDataSource = tableListDataSource.map((item) => {
          if (item.key === key) {
            newUser = { ...item, name, ipAddress, phoneNumber, about, signature };
            return { ...item, name, ipAddress, phoneNumber, about, signature };
          }
          return item;
        });
        return res.json(newUser);
      })();
      return;
    default:
      break;
  }

  const result = {
    list: tableListDataSource,
    pagination: {
      total: tableListDataSource.length,
    },
  };

  res.json(result);
}

export default {
  'GET /api/rule': getRule,
  'POST /api/rule': postRule,
};
