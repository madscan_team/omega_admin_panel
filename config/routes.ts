﻿export default [
  {
    path: '/user',
    layout: false,
    routes: [
      {
        path: '/user',
        routes: [
          {
            name: 'login',
            path: '/user/login',
            component: './User/login',
          },
        ],
      },
    ],
  },
  {
    path: '/welcome',
    name: 'welcome',
    icon: 'smile',
    component: './Welcome',
  },
  {
    path: '/admin',
    name: 'admin',
    icon: 'crown',
    access: 'canAdmin',
    component: './Admin',
    routes: [
      {
        path: '/admin/sub-page',
        name: 'sub-page',
        icon: 'smile',
        component: './Welcome',
      },
    ],
  },
  {
    name: 'Manage Users',
    icon: 'user',
    path: '/users',
    component: './ManageUsers',
  },
  {
    name: 'Manage Widgets',
    icon: 'alert',
    path: '/widgets',
    component: './ManageWidgets',
  },
  {
    name: 'Manage Features',
    icon: 'gift',
    path: '/features',
    component: './ManageFeatures',
  },
  {
    path: '/',
    redirect: '/welcome',
  },
  {
    component: './404',
  },
];
