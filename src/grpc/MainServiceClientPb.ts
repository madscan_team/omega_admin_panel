/**
 * @fileoverview gRPC-Web generated client stub for 
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck


import * as grpcWeb from 'grpc-web';

import * as main_pb from './main_pb';


export class MainServiceClient {
  client_: grpcWeb.AbstractClientBase;
  hostname_: string;
  credentials_: null | { [index: string]: string; };
  options_: null | { [index: string]: any; };

  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; }) {
    if (!options) options = {};
    if (!credentials) credentials = {};
    options['format'] = 'text';

    this.client_ = new grpcWeb.GrpcWebClientBase(options);
    this.hostname_ = hostname;
    this.credentials_ = credentials;
    this.options_ = options;
  }

  methodInfoGetAllUser = new grpcWeb.AbstractClientBase.MethodInfo(
    main_pb.UsersResponse,
    (request: main_pb.Empty) => {
      return request.serializeBinary();
    },
    main_pb.UsersResponse.deserializeBinary
  );

  getAllUser(
    request: main_pb.Empty,
    metadata: grpcWeb.Metadata | null): Promise<main_pb.UsersResponse>;

  getAllUser(
    request: main_pb.Empty,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: main_pb.UsersResponse) => void): grpcWeb.ClientReadableStream<main_pb.UsersResponse>;

  getAllUser(
    request: main_pb.Empty,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: main_pb.UsersResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/MainService/GetAllUser',
        request,
        metadata || {},
        this.methodInfoGetAllUser,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/MainService/GetAllUser',
    request,
    metadata || {},
    this.methodInfoGetAllUser);
  }

  methodInfoGetUserById = new grpcWeb.AbstractClientBase.MethodInfo(
    main_pb.UserResponse,
    (request: main_pb.UserRequestId) => {
      return request.serializeBinary();
    },
    main_pb.UserResponse.deserializeBinary
  );

  getUserById(
    request: main_pb.UserRequestId,
    metadata: grpcWeb.Metadata | null): Promise<main_pb.UserResponse>;

  getUserById(
    request: main_pb.UserRequestId,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: main_pb.UserResponse) => void): grpcWeb.ClientReadableStream<main_pb.UserResponse>;

  getUserById(
    request: main_pb.UserRequestId,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: main_pb.UserResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/MainService/GetUserById',
        request,
        metadata || {},
        this.methodInfoGetUserById,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/MainService/GetUserById',
    request,
    metadata || {},
    this.methodInfoGetUserById);
  }

  methodInfoCreateUser = new grpcWeb.AbstractClientBase.MethodInfo(
    main_pb.UserResponse,
    (request: main_pb.CreateUserRequest) => {
      return request.serializeBinary();
    },
    main_pb.UserResponse.deserializeBinary
  );

  createUser(
    request: main_pb.CreateUserRequest,
    metadata: grpcWeb.Metadata | null): Promise<main_pb.UserResponse>;

  createUser(
    request: main_pb.CreateUserRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: main_pb.UserResponse) => void): grpcWeb.ClientReadableStream<main_pb.UserResponse>;

  createUser(
    request: main_pb.CreateUserRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: main_pb.UserResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/MainService/CreateUser',
        request,
        metadata || {},
        this.methodInfoCreateUser,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/MainService/CreateUser',
    request,
    metadata || {},
    this.methodInfoCreateUser);
  }

  methodInfoUpdateUserById = new grpcWeb.AbstractClientBase.MethodInfo(
    main_pb.UserResponse,
    (request: main_pb.UpdateUserRequest) => {
      return request.serializeBinary();
    },
    main_pb.UserResponse.deserializeBinary
  );

  updateUserById(
    request: main_pb.UpdateUserRequest,
    metadata: grpcWeb.Metadata | null): Promise<main_pb.UserResponse>;

  updateUserById(
    request: main_pb.UpdateUserRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: main_pb.UserResponse) => void): grpcWeb.ClientReadableStream<main_pb.UserResponse>;

  updateUserById(
    request: main_pb.UpdateUserRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: main_pb.UserResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/MainService/UpdateUserById',
        request,
        metadata || {},
        this.methodInfoUpdateUserById,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/MainService/UpdateUserById',
    request,
    metadata || {},
    this.methodInfoUpdateUserById);
  }

  methodInfoDeleteUserById = new grpcWeb.AbstractClientBase.MethodInfo(
    main_pb.UserResponse,
    (request: main_pb.UserRequestId) => {
      return request.serializeBinary();
    },
    main_pb.UserResponse.deserializeBinary
  );

  deleteUserById(
    request: main_pb.UserRequestId,
    metadata: grpcWeb.Metadata | null): Promise<main_pb.UserResponse>;

  deleteUserById(
    request: main_pb.UserRequestId,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: main_pb.UserResponse) => void): grpcWeb.ClientReadableStream<main_pb.UserResponse>;

  deleteUserById(
    request: main_pb.UserRequestId,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: main_pb.UserResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/MainService/DeleteUserById',
        request,
        metadata || {},
        this.methodInfoDeleteUserById,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/MainService/DeleteUserById',
    request,
    metadata || {},
    this.methodInfoDeleteUserById);
  }

  methodInfoGetAllRole = new grpcWeb.AbstractClientBase.MethodInfo(
    main_pb.RolesResponse,
    (request: main_pb.Empty) => {
      return request.serializeBinary();
    },
    main_pb.RolesResponse.deserializeBinary
  );

  getAllRole(
    request: main_pb.Empty,
    metadata: grpcWeb.Metadata | null): Promise<main_pb.RolesResponse>;

  getAllRole(
    request: main_pb.Empty,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: main_pb.RolesResponse) => void): grpcWeb.ClientReadableStream<main_pb.RolesResponse>;

  getAllRole(
    request: main_pb.Empty,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: main_pb.RolesResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/MainService/GetAllRole',
        request,
        metadata || {},
        this.methodInfoGetAllRole,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/MainService/GetAllRole',
    request,
    metadata || {},
    this.methodInfoGetAllRole);
  }

  methodInfoGetRoleById = new grpcWeb.AbstractClientBase.MethodInfo(
    main_pb.RoleResponse,
    (request: main_pb.RoleRequestId) => {
      return request.serializeBinary();
    },
    main_pb.RoleResponse.deserializeBinary
  );

  getRoleById(
    request: main_pb.RoleRequestId,
    metadata: grpcWeb.Metadata | null): Promise<main_pb.RoleResponse>;

  getRoleById(
    request: main_pb.RoleRequestId,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: main_pb.RoleResponse) => void): grpcWeb.ClientReadableStream<main_pb.RoleResponse>;

  getRoleById(
    request: main_pb.RoleRequestId,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: main_pb.RoleResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/MainService/GetRoleById',
        request,
        metadata || {},
        this.methodInfoGetRoleById,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/MainService/GetRoleById',
    request,
    metadata || {},
    this.methodInfoGetRoleById);
  }

  methodInfoCreateRole = new grpcWeb.AbstractClientBase.MethodInfo(
    main_pb.RoleResponse,
    (request: main_pb.Role) => {
      return request.serializeBinary();
    },
    main_pb.RoleResponse.deserializeBinary
  );

  createRole(
    request: main_pb.Role,
    metadata: grpcWeb.Metadata | null): Promise<main_pb.RoleResponse>;

  createRole(
    request: main_pb.Role,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: main_pb.RoleResponse) => void): grpcWeb.ClientReadableStream<main_pb.RoleResponse>;

  createRole(
    request: main_pb.Role,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: main_pb.RoleResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/MainService/CreateRole',
        request,
        metadata || {},
        this.methodInfoCreateRole,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/MainService/CreateRole',
    request,
    metadata || {},
    this.methodInfoCreateRole);
  }

  methodInfoUpdateRoleById = new grpcWeb.AbstractClientBase.MethodInfo(
    main_pb.RoleResponse,
    (request: main_pb.Role) => {
      return request.serializeBinary();
    },
    main_pb.RoleResponse.deserializeBinary
  );

  updateRoleById(
    request: main_pb.Role,
    metadata: grpcWeb.Metadata | null): Promise<main_pb.RoleResponse>;

  updateRoleById(
    request: main_pb.Role,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: main_pb.RoleResponse) => void): grpcWeb.ClientReadableStream<main_pb.RoleResponse>;

  updateRoleById(
    request: main_pb.Role,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: main_pb.RoleResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/MainService/UpdateRoleById',
        request,
        metadata || {},
        this.methodInfoUpdateRoleById,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/MainService/UpdateRoleById',
    request,
    metadata || {},
    this.methodInfoUpdateRoleById);
  }

  methodInfoDeleteRoleById = new grpcWeb.AbstractClientBase.MethodInfo(
    main_pb.RoleResponse,
    (request: main_pb.RoleRequestId) => {
      return request.serializeBinary();
    },
    main_pb.RoleResponse.deserializeBinary
  );

  deleteRoleById(
    request: main_pb.RoleRequestId,
    metadata: grpcWeb.Metadata | null): Promise<main_pb.RoleResponse>;

  deleteRoleById(
    request: main_pb.RoleRequestId,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: main_pb.RoleResponse) => void): grpcWeb.ClientReadableStream<main_pb.RoleResponse>;

  deleteRoleById(
    request: main_pb.RoleRequestId,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: main_pb.RoleResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/MainService/DeleteRoleById',
        request,
        metadata || {},
        this.methodInfoDeleteRoleById,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/MainService/DeleteRoleById',
    request,
    metadata || {},
    this.methodInfoDeleteRoleById);
  }

  methodInfoGetAllFeature = new grpcWeb.AbstractClientBase.MethodInfo(
    main_pb.FeaturesResponse,
    (request: main_pb.Empty) => {
      return request.serializeBinary();
    },
    main_pb.FeaturesResponse.deserializeBinary
  );

  getAllFeature(
    request: main_pb.Empty,
    metadata: grpcWeb.Metadata | null): Promise<main_pb.FeaturesResponse>;

  getAllFeature(
    request: main_pb.Empty,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: main_pb.FeaturesResponse) => void): grpcWeb.ClientReadableStream<main_pb.FeaturesResponse>;

  getAllFeature(
    request: main_pb.Empty,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: main_pb.FeaturesResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/MainService/GetAllFeature',
        request,
        metadata || {},
        this.methodInfoGetAllFeature,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/MainService/GetAllFeature',
    request,
    metadata || {},
    this.methodInfoGetAllFeature);
  }

  methodInfoGetFeatureById = new grpcWeb.AbstractClientBase.MethodInfo(
    main_pb.FeatureResponse,
    (request: main_pb.FeatureRequestId) => {
      return request.serializeBinary();
    },
    main_pb.FeatureResponse.deserializeBinary
  );

  getFeatureById(
    request: main_pb.FeatureRequestId,
    metadata: grpcWeb.Metadata | null): Promise<main_pb.FeatureResponse>;

  getFeatureById(
    request: main_pb.FeatureRequestId,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: main_pb.FeatureResponse) => void): grpcWeb.ClientReadableStream<main_pb.FeatureResponse>;

  getFeatureById(
    request: main_pb.FeatureRequestId,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: main_pb.FeatureResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/MainService/GetFeatureById',
        request,
        metadata || {},
        this.methodInfoGetFeatureById,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/MainService/GetFeatureById',
    request,
    metadata || {},
    this.methodInfoGetFeatureById);
  }

  methodInfoCreateFeature = new grpcWeb.AbstractClientBase.MethodInfo(
    main_pb.FeatureResponse,
    (request: main_pb.Feature) => {
      return request.serializeBinary();
    },
    main_pb.FeatureResponse.deserializeBinary
  );

  createFeature(
    request: main_pb.Feature,
    metadata: grpcWeb.Metadata | null): Promise<main_pb.FeatureResponse>;

  createFeature(
    request: main_pb.Feature,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: main_pb.FeatureResponse) => void): grpcWeb.ClientReadableStream<main_pb.FeatureResponse>;

  createFeature(
    request: main_pb.Feature,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: main_pb.FeatureResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/MainService/CreateFeature',
        request,
        metadata || {},
        this.methodInfoCreateFeature,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/MainService/CreateFeature',
    request,
    metadata || {},
    this.methodInfoCreateFeature);
  }

  methodInfoUpdateFeatureById = new grpcWeb.AbstractClientBase.MethodInfo(
    main_pb.FeatureResponse,
    (request: main_pb.Feature) => {
      return request.serializeBinary();
    },
    main_pb.FeatureResponse.deserializeBinary
  );

  updateFeatureById(
    request: main_pb.Feature,
    metadata: grpcWeb.Metadata | null): Promise<main_pb.FeatureResponse>;

  updateFeatureById(
    request: main_pb.Feature,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: main_pb.FeatureResponse) => void): grpcWeb.ClientReadableStream<main_pb.FeatureResponse>;

  updateFeatureById(
    request: main_pb.Feature,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: main_pb.FeatureResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/MainService/UpdateFeatureById',
        request,
        metadata || {},
        this.methodInfoUpdateFeatureById,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/MainService/UpdateFeatureById',
    request,
    metadata || {},
    this.methodInfoUpdateFeatureById);
  }

  methodInfoDeleteFeatureById = new grpcWeb.AbstractClientBase.MethodInfo(
    main_pb.FeatureResponse,
    (request: main_pb.FeatureRequestId) => {
      return request.serializeBinary();
    },
    main_pb.FeatureResponse.deserializeBinary
  );

  deleteFeatureById(
    request: main_pb.FeatureRequestId,
    metadata: grpcWeb.Metadata | null): Promise<main_pb.FeatureResponse>;

  deleteFeatureById(
    request: main_pb.FeatureRequestId,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: main_pb.FeatureResponse) => void): grpcWeb.ClientReadableStream<main_pb.FeatureResponse>;

  deleteFeatureById(
    request: main_pb.FeatureRequestId,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: main_pb.FeatureResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/MainService/DeleteFeatureById',
        request,
        metadata || {},
        this.methodInfoDeleteFeatureById,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/MainService/DeleteFeatureById',
    request,
    metadata || {},
    this.methodInfoDeleteFeatureById);
  }

}

