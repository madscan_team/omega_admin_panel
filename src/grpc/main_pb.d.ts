import * as jspb from 'google-protobuf'



export class UsersResponse extends jspb.Message {
  getSuccess(): boolean;
  setSuccess(value: boolean): UsersResponse;

  getStatus(): number;
  setStatus(value: number): UsersResponse;

  getMessage(): string;
  setMessage(value: string): UsersResponse;

  getData(): UsersResponseData | undefined;
  setData(value?: UsersResponseData): UsersResponse;
  hasData(): boolean;
  clearData(): UsersResponse;

  getStatusCase(): UsersResponse.StatusCase;

  getMessageCase(): UsersResponse.MessageCase;

  getDataCase(): UsersResponse.DataCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UsersResponse.AsObject;
  static toObject(includeInstance: boolean, msg: UsersResponse): UsersResponse.AsObject;
  static serializeBinaryToWriter(message: UsersResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UsersResponse;
  static deserializeBinaryFromReader(message: UsersResponse, reader: jspb.BinaryReader): UsersResponse;
}

export namespace UsersResponse {
  export type AsObject = {
    success: boolean,
    status: number,
    message: string,
    data?: UsersResponseData.AsObject,
  }

  export enum StatusCase { 
    _STATUS_NOT_SET = 0,
    STATUS = 2,
  }

  export enum MessageCase { 
    _MESSAGE_NOT_SET = 0,
    MESSAGE = 3,
  }

  export enum DataCase { 
    _DATA_NOT_SET = 0,
    DATA = 4,
  }
}

export class UsersResponseData extends jspb.Message {
  getUsersList(): Array<User>;
  setUsersList(value: Array<User>): UsersResponseData;
  clearUsersList(): UsersResponseData;
  addUsers(value?: User, index?: number): User;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UsersResponseData.AsObject;
  static toObject(includeInstance: boolean, msg: UsersResponseData): UsersResponseData.AsObject;
  static serializeBinaryToWriter(message: UsersResponseData, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UsersResponseData;
  static deserializeBinaryFromReader(message: UsersResponseData, reader: jspb.BinaryReader): UsersResponseData;
}

export namespace UsersResponseData {
  export type AsObject = {
    usersList: Array<User.AsObject>,
  }
}

export class UserResponse extends jspb.Message {
  getSuccess(): boolean;
  setSuccess(value: boolean): UserResponse;

  getStatus(): number;
  setStatus(value: number): UserResponse;

  getMessage(): string;
  setMessage(value: string): UserResponse;

  getData(): UserResponseData | undefined;
  setData(value?: UserResponseData): UserResponse;
  hasData(): boolean;
  clearData(): UserResponse;

  getStatusCase(): UserResponse.StatusCase;

  getMessageCase(): UserResponse.MessageCase;

  getDataCase(): UserResponse.DataCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserResponse.AsObject;
  static toObject(includeInstance: boolean, msg: UserResponse): UserResponse.AsObject;
  static serializeBinaryToWriter(message: UserResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserResponse;
  static deserializeBinaryFromReader(message: UserResponse, reader: jspb.BinaryReader): UserResponse;
}

export namespace UserResponse {
  export type AsObject = {
    success: boolean,
    status: number,
    message: string,
    data?: UserResponseData.AsObject,
  }

  export enum StatusCase { 
    _STATUS_NOT_SET = 0,
    STATUS = 2,
  }

  export enum MessageCase { 
    _MESSAGE_NOT_SET = 0,
    MESSAGE = 3,
  }

  export enum DataCase { 
    _DATA_NOT_SET = 0,
    DATA = 4,
  }
}

export class UserResponseData extends jspb.Message {
  getUser(): User | undefined;
  setUser(value?: User): UserResponseData;
  hasUser(): boolean;
  clearUser(): UserResponseData;

  getUserCase(): UserResponseData.UserCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserResponseData.AsObject;
  static toObject(includeInstance: boolean, msg: UserResponseData): UserResponseData.AsObject;
  static serializeBinaryToWriter(message: UserResponseData, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserResponseData;
  static deserializeBinaryFromReader(message: UserResponseData, reader: jspb.BinaryReader): UserResponseData;
}

export namespace UserResponseData {
  export type AsObject = {
    user?: User.AsObject,
  }

  export enum UserCase { 
    _USER_NOT_SET = 0,
    USER = 1,
  }
}

export class Empty extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Empty.AsObject;
  static toObject(includeInstance: boolean, msg: Empty): Empty.AsObject;
  static serializeBinaryToWriter(message: Empty, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Empty;
  static deserializeBinaryFromReader(message: Empty, reader: jspb.BinaryReader): Empty;
}

export namespace Empty {
  export type AsObject = {
  }
}

export class User extends jspb.Message {
  getId(): string;
  setId(value: string): User;

  getFirstname(): string;
  setFirstname(value: string): User;

  getLastname(): string;
  setLastname(value: string): User;

  getUsername(): string;
  setUsername(value: string): User;

  getEmail(): string;
  setEmail(value: string): User;

  getPassword(): string;
  setPassword(value: string): User;

  getPhone(): number;
  setPhone(value: number): User;

  getRole(): Role | undefined;
  setRole(value?: Role): User;
  hasRole(): boolean;
  clearRole(): User;

  getGoogleprofileurl(): string;
  setGoogleprofileurl(value: string): User;

  getUid(): string;
  setUid(value: string): User;

  getSignature(): string;
  setSignature(value: string): User;

  getAbout(): string;
  setAbout(value: string): User;

  getRocketchatid(): string;
  setRocketchatid(value: string): User;

  getFaqList(): Array<Faq>;
  setFaqList(value: Array<Faq>): User;
  clearFaqList(): User;
  addFaq(value?: Faq, index?: number): Faq;

  getRocketchatauthtoken(): string;
  setRocketchatauthtoken(value: string): User;

  getClientip(): string;
  setClientip(value: string): User;

  getExpirytoken(): string;
  setExpirytoken(value: string): User;

  getUserpermission(): number;
  setUserpermission(value: number): User;

  getFeedpermission(): number;
  setFeedpermission(value: number): User;

  getApplicationsList(): Array<string>;
  setApplicationsList(value: Array<string>): User;
  clearApplicationsList(): User;
  addApplications(value: string, index?: number): User;

  getApplicationfeaturepermissionsMap(): jspb.Map<string, string>;
  clearApplicationfeaturepermissionsMap(): User;

  getGoogleprofileurlCase(): User.GoogleprofileurlCase;

  getUidCase(): User.UidCase;

  getSignatureCase(): User.SignatureCase;

  getAboutCase(): User.AboutCase;

  getRocketchatidCase(): User.RocketchatidCase;

  getRocketchatauthtokenCase(): User.RocketchatauthtokenCase;

  getClientipCase(): User.ClientipCase;

  getExpirytokenCase(): User.ExpirytokenCase;

  getUserpermissionCase(): User.UserpermissionCase;

  getFeedpermissionCase(): User.FeedpermissionCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): User.AsObject;
  static toObject(includeInstance: boolean, msg: User): User.AsObject;
  static serializeBinaryToWriter(message: User, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): User;
  static deserializeBinaryFromReader(message: User, reader: jspb.BinaryReader): User;
}

export namespace User {
  export type AsObject = {
    id: string,
    firstname: string,
    lastname: string,
    username: string,
    email: string,
    password: string,
    phone: number,
    role?: Role.AsObject,
    googleprofileurl: string,
    uid: string,
    signature: string,
    about: string,
    rocketchatid: string,
    faqList: Array<Faq.AsObject>,
    rocketchatauthtoken: string,
    clientip: string,
    expirytoken: string,
    userpermission: number,
    feedpermission: number,
    applicationsList: Array<string>,
    applicationfeaturepermissionsMap: Array<[string, string]>,
  }

  export enum GoogleprofileurlCase { 
    _GOOGLEPROFILEURL_NOT_SET = 0,
    GOOGLEPROFILEURL = 9,
  }

  export enum UidCase { 
    _UID_NOT_SET = 0,
    UID = 10,
  }

  export enum SignatureCase { 
    _SIGNATURE_NOT_SET = 0,
    SIGNATURE = 11,
  }

  export enum AboutCase { 
    _ABOUT_NOT_SET = 0,
    ABOUT = 12,
  }

  export enum RocketchatidCase { 
    _ROCKETCHATID_NOT_SET = 0,
    ROCKETCHATID = 13,
  }

  export enum RocketchatauthtokenCase { 
    _ROCKETCHATAUTHTOKEN_NOT_SET = 0,
    ROCKETCHATAUTHTOKEN = 15,
  }

  export enum ClientipCase { 
    _CLIENTIP_NOT_SET = 0,
    CLIENTIP = 16,
  }

  export enum ExpirytokenCase { 
    _EXPIRYTOKEN_NOT_SET = 0,
    EXPIRYTOKEN = 17,
  }

  export enum UserpermissionCase { 
    _USERPERMISSION_NOT_SET = 0,
    USERPERMISSION = 18,
  }

  export enum FeedpermissionCase { 
    _FEEDPERMISSION_NOT_SET = 0,
    FEEDPERMISSION = 19,
  }
}

export class List extends jspb.Message {
  getKey(): string;
  setKey(value: string): List;

  getPermissionsList(): Array<number>;
  setPermissionsList(value: Array<number>): List;
  clearPermissionsList(): List;
  addPermissions(value: number, index?: number): List;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): List.AsObject;
  static toObject(includeInstance: boolean, msg: List): List.AsObject;
  static serializeBinaryToWriter(message: List, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): List;
  static deserializeBinaryFromReader(message: List, reader: jspb.BinaryReader): List;
}

export namespace List {
  export type AsObject = {
    key: string,
    permissionsList: Array<number>,
  }
}

export class Faq extends jspb.Message {
  getQuestion(): string;
  setQuestion(value: string): Faq;

  getAnswer(): string;
  setAnswer(value: string): Faq;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Faq.AsObject;
  static toObject(includeInstance: boolean, msg: Faq): Faq.AsObject;
  static serializeBinaryToWriter(message: Faq, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Faq;
  static deserializeBinaryFromReader(message: Faq, reader: jspb.BinaryReader): Faq;
}

export namespace Faq {
  export type AsObject = {
    question: string,
    answer: string,
  }
}

export class Question extends jspb.Message {
  getId(): string;
  setId(value: string): Question;

  getQuestion(): string;
  setQuestion(value: string): Question;

  getAnswerList(): Array<string>;
  setAnswerList(value: Array<string>): Question;
  clearAnswerList(): Question;
  addAnswer(value: string, index?: number): Question;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Question.AsObject;
  static toObject(includeInstance: boolean, msg: Question): Question.AsObject;
  static serializeBinaryToWriter(message: Question, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Question;
  static deserializeBinaryFromReader(message: Question, reader: jspb.BinaryReader): Question;
}

export namespace Question {
  export type AsObject = {
    id: string,
    question: string,
    answerList: Array<string>,
  }
}

export class CreateUserRequest extends jspb.Message {
  getId(): string;
  setId(value: string): CreateUserRequest;

  getFirstname(): string;
  setFirstname(value: string): CreateUserRequest;

  getLastname(): string;
  setLastname(value: string): CreateUserRequest;

  getUsername(): string;
  setUsername(value: string): CreateUserRequest;

  getEmail(): string;
  setEmail(value: string): CreateUserRequest;

  getPassword(): string;
  setPassword(value: string): CreateUserRequest;

  getPhone(): number;
  setPhone(value: number): CreateUserRequest;

  getRole(): string;
  setRole(value: string): CreateUserRequest;

  getGoogleprofileurl(): string;
  setGoogleprofileurl(value: string): CreateUserRequest;

  getUid(): string;
  setUid(value: string): CreateUserRequest;

  getSignature(): string;
  setSignature(value: string): CreateUserRequest;

  getAbout(): string;
  setAbout(value: string): CreateUserRequest;

  getRocketchatid(): string;
  setRocketchatid(value: string): CreateUserRequest;

  getFaqList(): Array<Faq>;
  setFaqList(value: Array<Faq>): CreateUserRequest;
  clearFaqList(): CreateUserRequest;
  addFaq(value?: Faq, index?: number): Faq;

  getRocketchatauthtoken(): string;
  setRocketchatauthtoken(value: string): CreateUserRequest;

  getClientip(): string;
  setClientip(value: string): CreateUserRequest;

  getExpirytoken(): string;
  setExpirytoken(value: string): CreateUserRequest;

  getUserpermission(): number;
  setUserpermission(value: number): CreateUserRequest;

  getFeedpermission(): number;
  setFeedpermission(value: number): CreateUserRequest;

  getApplicationsList(): Array<string>;
  setApplicationsList(value: Array<string>): CreateUserRequest;
  clearApplicationsList(): CreateUserRequest;
  addApplications(value: string, index?: number): CreateUserRequest;

  getApplicationfeaturepermissionsMap(): jspb.Map<string, string>;
  clearApplicationfeaturepermissionsMap(): CreateUserRequest;

  getRoleCase(): CreateUserRequest.RoleCase;

  getGoogleprofileurlCase(): CreateUserRequest.GoogleprofileurlCase;

  getUidCase(): CreateUserRequest.UidCase;

  getSignatureCase(): CreateUserRequest.SignatureCase;

  getAboutCase(): CreateUserRequest.AboutCase;

  getRocketchatidCase(): CreateUserRequest.RocketchatidCase;

  getRocketchatauthtokenCase(): CreateUserRequest.RocketchatauthtokenCase;

  getClientipCase(): CreateUserRequest.ClientipCase;

  getExpirytokenCase(): CreateUserRequest.ExpirytokenCase;

  getUserpermissionCase(): CreateUserRequest.UserpermissionCase;

  getFeedpermissionCase(): CreateUserRequest.FeedpermissionCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateUserRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateUserRequest): CreateUserRequest.AsObject;
  static serializeBinaryToWriter(message: CreateUserRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateUserRequest;
  static deserializeBinaryFromReader(message: CreateUserRequest, reader: jspb.BinaryReader): CreateUserRequest;
}

export namespace CreateUserRequest {
  export type AsObject = {
    id: string,
    firstname: string,
    lastname: string,
    username: string,
    email: string,
    password: string,
    phone: number,
    role: string,
    googleprofileurl: string,
    uid: string,
    signature: string,
    about: string,
    rocketchatid: string,
    faqList: Array<Faq.AsObject>,
    rocketchatauthtoken: string,
    clientip: string,
    expirytoken: string,
    userpermission: number,
    feedpermission: number,
    applicationsList: Array<string>,
    applicationfeaturepermissionsMap: Array<[string, string]>,
  }

  export enum RoleCase { 
    _ROLE_NOT_SET = 0,
    ROLE = 8,
  }

  export enum GoogleprofileurlCase { 
    _GOOGLEPROFILEURL_NOT_SET = 0,
    GOOGLEPROFILEURL = 9,
  }

  export enum UidCase { 
    _UID_NOT_SET = 0,
    UID = 10,
  }

  export enum SignatureCase { 
    _SIGNATURE_NOT_SET = 0,
    SIGNATURE = 11,
  }

  export enum AboutCase { 
    _ABOUT_NOT_SET = 0,
    ABOUT = 12,
  }

  export enum RocketchatidCase { 
    _ROCKETCHATID_NOT_SET = 0,
    ROCKETCHATID = 13,
  }

  export enum RocketchatauthtokenCase { 
    _ROCKETCHATAUTHTOKEN_NOT_SET = 0,
    ROCKETCHATAUTHTOKEN = 15,
  }

  export enum ClientipCase { 
    _CLIENTIP_NOT_SET = 0,
    CLIENTIP = 16,
  }

  export enum ExpirytokenCase { 
    _EXPIRYTOKEN_NOT_SET = 0,
    EXPIRYTOKEN = 17,
  }

  export enum UserpermissionCase { 
    _USERPERMISSION_NOT_SET = 0,
    USERPERMISSION = 18,
  }

  export enum FeedpermissionCase { 
    _FEEDPERMISSION_NOT_SET = 0,
    FEEDPERMISSION = 19,
  }
}

export class UpdateUserRequest extends jspb.Message {
  getId(): string;
  setId(value: string): UpdateUserRequest;

  getFirstname(): string;
  setFirstname(value: string): UpdateUserRequest;

  getLastname(): string;
  setLastname(value: string): UpdateUserRequest;

  getUsername(): string;
  setUsername(value: string): UpdateUserRequest;

  getEmail(): string;
  setEmail(value: string): UpdateUserRequest;

  getPassword(): string;
  setPassword(value: string): UpdateUserRequest;

  getPhone(): number;
  setPhone(value: number): UpdateUserRequest;

  getRole(): string;
  setRole(value: string): UpdateUserRequest;

  getGoogleprofileurl(): string;
  setGoogleprofileurl(value: string): UpdateUserRequest;

  getUid(): string;
  setUid(value: string): UpdateUserRequest;

  getSignature(): string;
  setSignature(value: string): UpdateUserRequest;

  getAbout(): string;
  setAbout(value: string): UpdateUserRequest;

  getRocketchatid(): string;
  setRocketchatid(value: string): UpdateUserRequest;

  getFaqList(): Array<Faq>;
  setFaqList(value: Array<Faq>): UpdateUserRequest;
  clearFaqList(): UpdateUserRequest;
  addFaq(value?: Faq, index?: number): Faq;

  getRocketchatauthtoken(): string;
  setRocketchatauthtoken(value: string): UpdateUserRequest;

  getClientip(): string;
  setClientip(value: string): UpdateUserRequest;

  getExpirytoken(): string;
  setExpirytoken(value: string): UpdateUserRequest;

  getUserpermission(): number;
  setUserpermission(value: number): UpdateUserRequest;

  getFeedpermission(): number;
  setFeedpermission(value: number): UpdateUserRequest;

  getApplicationsList(): Array<string>;
  setApplicationsList(value: Array<string>): UpdateUserRequest;
  clearApplicationsList(): UpdateUserRequest;
  addApplications(value: string, index?: number): UpdateUserRequest;

  getApplicationfeaturepermissionsMap(): jspb.Map<string, string>;
  clearApplicationfeaturepermissionsMap(): UpdateUserRequest;

  getFirstnameCase(): UpdateUserRequest.FirstnameCase;

  getLastnameCase(): UpdateUserRequest.LastnameCase;

  getUsernameCase(): UpdateUserRequest.UsernameCase;

  getEmailCase(): UpdateUserRequest.EmailCase;

  getPasswordCase(): UpdateUserRequest.PasswordCase;

  getPhoneCase(): UpdateUserRequest.PhoneCase;

  getRoleCase(): UpdateUserRequest.RoleCase;

  getGoogleprofileurlCase(): UpdateUserRequest.GoogleprofileurlCase;

  getUidCase(): UpdateUserRequest.UidCase;

  getSignatureCase(): UpdateUserRequest.SignatureCase;

  getAboutCase(): UpdateUserRequest.AboutCase;

  getRocketchatidCase(): UpdateUserRequest.RocketchatidCase;

  getRocketchatauthtokenCase(): UpdateUserRequest.RocketchatauthtokenCase;

  getClientipCase(): UpdateUserRequest.ClientipCase;

  getExpirytokenCase(): UpdateUserRequest.ExpirytokenCase;

  getUserpermissionCase(): UpdateUserRequest.UserpermissionCase;

  getFeedpermissionCase(): UpdateUserRequest.FeedpermissionCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateUserRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateUserRequest): UpdateUserRequest.AsObject;
  static serializeBinaryToWriter(message: UpdateUserRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateUserRequest;
  static deserializeBinaryFromReader(message: UpdateUserRequest, reader: jspb.BinaryReader): UpdateUserRequest;
}

export namespace UpdateUserRequest {
  export type AsObject = {
    id: string,
    firstname: string,
    lastname: string,
    username: string,
    email: string,
    password: string,
    phone: number,
    role: string,
    googleprofileurl: string,
    uid: string,
    signature: string,
    about: string,
    rocketchatid: string,
    faqList: Array<Faq.AsObject>,
    rocketchatauthtoken: string,
    clientip: string,
    expirytoken: string,
    userpermission: number,
    feedpermission: number,
    applicationsList: Array<string>,
    applicationfeaturepermissionsMap: Array<[string, string]>,
  }

  export enum FirstnameCase { 
    _FIRSTNAME_NOT_SET = 0,
    FIRSTNAME = 2,
  }

  export enum LastnameCase { 
    _LASTNAME_NOT_SET = 0,
    LASTNAME = 3,
  }

  export enum UsernameCase { 
    _USERNAME_NOT_SET = 0,
    USERNAME = 4,
  }

  export enum EmailCase { 
    _EMAIL_NOT_SET = 0,
    EMAIL = 5,
  }

  export enum PasswordCase { 
    _PASSWORD_NOT_SET = 0,
    PASSWORD = 6,
  }

  export enum PhoneCase { 
    _PHONE_NOT_SET = 0,
    PHONE = 7,
  }

  export enum RoleCase { 
    _ROLE_NOT_SET = 0,
    ROLE = 8,
  }

  export enum GoogleprofileurlCase { 
    _GOOGLEPROFILEURL_NOT_SET = 0,
    GOOGLEPROFILEURL = 9,
  }

  export enum UidCase { 
    _UID_NOT_SET = 0,
    UID = 10,
  }

  export enum SignatureCase { 
    _SIGNATURE_NOT_SET = 0,
    SIGNATURE = 11,
  }

  export enum AboutCase { 
    _ABOUT_NOT_SET = 0,
    ABOUT = 12,
  }

  export enum RocketchatidCase { 
    _ROCKETCHATID_NOT_SET = 0,
    ROCKETCHATID = 13,
  }

  export enum RocketchatauthtokenCase { 
    _ROCKETCHATAUTHTOKEN_NOT_SET = 0,
    ROCKETCHATAUTHTOKEN = 15,
  }

  export enum ClientipCase { 
    _CLIENTIP_NOT_SET = 0,
    CLIENTIP = 16,
  }

  export enum ExpirytokenCase { 
    _EXPIRYTOKEN_NOT_SET = 0,
    EXPIRYTOKEN = 17,
  }

  export enum UserpermissionCase { 
    _USERPERMISSION_NOT_SET = 0,
    USERPERMISSION = 18,
  }

  export enum FeedpermissionCase { 
    _FEEDPERMISSION_NOT_SET = 0,
    FEEDPERMISSION = 19,
  }
}

export class UserList extends jspb.Message {
  getUsersList(): Array<User>;
  setUsersList(value: Array<User>): UserList;
  clearUsersList(): UserList;
  addUsers(value?: User, index?: number): User;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserList.AsObject;
  static toObject(includeInstance: boolean, msg: UserList): UserList.AsObject;
  static serializeBinaryToWriter(message: UserList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserList;
  static deserializeBinaryFromReader(message: UserList, reader: jspb.BinaryReader): UserList;
}

export namespace UserList {
  export type AsObject = {
    usersList: Array<User.AsObject>,
  }
}

export class UserRequestId extends jspb.Message {
  getId(): string;
  setId(value: string): UserRequestId;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserRequestId.AsObject;
  static toObject(includeInstance: boolean, msg: UserRequestId): UserRequestId.AsObject;
  static serializeBinaryToWriter(message: UserRequestId, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserRequestId;
  static deserializeBinaryFromReader(message: UserRequestId, reader: jspb.BinaryReader): UserRequestId;
}

export namespace UserRequestId {
  export type AsObject = {
    id: string,
  }
}

export class Role extends jspb.Message {
  getId(): string;
  setId(value: string): Role;

  getEntitlement(): string;
  setEntitlement(value: string): Role;

  getPermissionsMap(): jspb.Map<string, string>;
  clearPermissionsMap(): Role;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Role.AsObject;
  static toObject(includeInstance: boolean, msg: Role): Role.AsObject;
  static serializeBinaryToWriter(message: Role, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Role;
  static deserializeBinaryFromReader(message: Role, reader: jspb.BinaryReader): Role;
}

export namespace Role {
  export type AsObject = {
    id: string,
    entitlement: string,
    permissionsMap: Array<[string, string]>,
  }
}

export class MapPermissions extends jspb.Message {
  getKey(): string;
  setKey(value: string): MapPermissions;

  getValue(): string;
  setValue(value: string): MapPermissions;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MapPermissions.AsObject;
  static toObject(includeInstance: boolean, msg: MapPermissions): MapPermissions.AsObject;
  static serializeBinaryToWriter(message: MapPermissions, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MapPermissions;
  static deserializeBinaryFromReader(message: MapPermissions, reader: jspb.BinaryReader): MapPermissions;
}

export namespace MapPermissions {
  export type AsObject = {
    key: string,
    value: string,
  }
}

export class RoleList extends jspb.Message {
  getRolesList(): Array<Role>;
  setRolesList(value: Array<Role>): RoleList;
  clearRolesList(): RoleList;
  addRoles(value?: Role, index?: number): Role;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RoleList.AsObject;
  static toObject(includeInstance: boolean, msg: RoleList): RoleList.AsObject;
  static serializeBinaryToWriter(message: RoleList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RoleList;
  static deserializeBinaryFromReader(message: RoleList, reader: jspb.BinaryReader): RoleList;
}

export namespace RoleList {
  export type AsObject = {
    rolesList: Array<Role.AsObject>,
  }
}

export class RoleRequestId extends jspb.Message {
  getId(): string;
  setId(value: string): RoleRequestId;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RoleRequestId.AsObject;
  static toObject(includeInstance: boolean, msg: RoleRequestId): RoleRequestId.AsObject;
  static serializeBinaryToWriter(message: RoleRequestId, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RoleRequestId;
  static deserializeBinaryFromReader(message: RoleRequestId, reader: jspb.BinaryReader): RoleRequestId;
}

export namespace RoleRequestId {
  export type AsObject = {
    id: string,
  }
}

export class RoleResponse extends jspb.Message {
  getSuccess(): boolean;
  setSuccess(value: boolean): RoleResponse;

  getStatus(): number;
  setStatus(value: number): RoleResponse;

  getMessage(): string;
  setMessage(value: string): RoleResponse;

  getData(): RoleResponseData | undefined;
  setData(value?: RoleResponseData): RoleResponse;
  hasData(): boolean;
  clearData(): RoleResponse;

  getStatusCase(): RoleResponse.StatusCase;

  getMessageCase(): RoleResponse.MessageCase;

  getDataCase(): RoleResponse.DataCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RoleResponse.AsObject;
  static toObject(includeInstance: boolean, msg: RoleResponse): RoleResponse.AsObject;
  static serializeBinaryToWriter(message: RoleResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RoleResponse;
  static deserializeBinaryFromReader(message: RoleResponse, reader: jspb.BinaryReader): RoleResponse;
}

export namespace RoleResponse {
  export type AsObject = {
    success: boolean,
    status: number,
    message: string,
    data?: RoleResponseData.AsObject,
  }

  export enum StatusCase { 
    _STATUS_NOT_SET = 0,
    STATUS = 2,
  }

  export enum MessageCase { 
    _MESSAGE_NOT_SET = 0,
    MESSAGE = 3,
  }

  export enum DataCase { 
    _DATA_NOT_SET = 0,
    DATA = 4,
  }
}

export class RoleResponseData extends jspb.Message {
  getRole(): Role | undefined;
  setRole(value?: Role): RoleResponseData;
  hasRole(): boolean;
  clearRole(): RoleResponseData;

  getRoleCase(): RoleResponseData.RoleCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RoleResponseData.AsObject;
  static toObject(includeInstance: boolean, msg: RoleResponseData): RoleResponseData.AsObject;
  static serializeBinaryToWriter(message: RoleResponseData, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RoleResponseData;
  static deserializeBinaryFromReader(message: RoleResponseData, reader: jspb.BinaryReader): RoleResponseData;
}

export namespace RoleResponseData {
  export type AsObject = {
    role?: Role.AsObject,
  }

  export enum RoleCase { 
    _ROLE_NOT_SET = 0,
    ROLE = 1,
  }
}

export class RolesResponse extends jspb.Message {
  getSuccess(): boolean;
  setSuccess(value: boolean): RolesResponse;

  getStatus(): number;
  setStatus(value: number): RolesResponse;

  getMessage(): string;
  setMessage(value: string): RolesResponse;

  getData(): RolesResponseData | undefined;
  setData(value?: RolesResponseData): RolesResponse;
  hasData(): boolean;
  clearData(): RolesResponse;

  getStatusCase(): RolesResponse.StatusCase;

  getMessageCase(): RolesResponse.MessageCase;

  getDataCase(): RolesResponse.DataCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RolesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: RolesResponse): RolesResponse.AsObject;
  static serializeBinaryToWriter(message: RolesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RolesResponse;
  static deserializeBinaryFromReader(message: RolesResponse, reader: jspb.BinaryReader): RolesResponse;
}

export namespace RolesResponse {
  export type AsObject = {
    success: boolean,
    status: number,
    message: string,
    data?: RolesResponseData.AsObject,
  }

  export enum StatusCase { 
    _STATUS_NOT_SET = 0,
    STATUS = 2,
  }

  export enum MessageCase { 
    _MESSAGE_NOT_SET = 0,
    MESSAGE = 3,
  }

  export enum DataCase { 
    _DATA_NOT_SET = 0,
    DATA = 4,
  }
}

export class RolesResponseData extends jspb.Message {
  getRolesList(): Array<Role>;
  setRolesList(value: Array<Role>): RolesResponseData;
  clearRolesList(): RolesResponseData;
  addRoles(value?: Role, index?: number): Role;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RolesResponseData.AsObject;
  static toObject(includeInstance: boolean, msg: RolesResponseData): RolesResponseData.AsObject;
  static serializeBinaryToWriter(message: RolesResponseData, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RolesResponseData;
  static deserializeBinaryFromReader(message: RolesResponseData, reader: jspb.BinaryReader): RolesResponseData;
}

export namespace RolesResponseData {
  export type AsObject = {
    rolesList: Array<Role.AsObject>,
  }
}

export class Feature extends jspb.Message {
  getId(): string;
  setId(value: string): Feature;

  getKey(): string;
  setKey(value: string): Feature;

  getValuetype(): string;
  setValuetype(value: string): Feature;

  getDefaultvalue(): string;
  setDefaultvalue(value: string): Feature;

  getLimit(): number;
  setLimit(value: number): Feature;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Feature.AsObject;
  static toObject(includeInstance: boolean, msg: Feature): Feature.AsObject;
  static serializeBinaryToWriter(message: Feature, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Feature;
  static deserializeBinaryFromReader(message: Feature, reader: jspb.BinaryReader): Feature;
}

export namespace Feature {
  export type AsObject = {
    id: string,
    key: string,
    valuetype: string,
    defaultvalue: string,
    limit: number,
  }
}

export class FeatureList extends jspb.Message {
  getFeaturesList(): Array<Feature>;
  setFeaturesList(value: Array<Feature>): FeatureList;
  clearFeaturesList(): FeatureList;
  addFeatures(value?: Feature, index?: number): Feature;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FeatureList.AsObject;
  static toObject(includeInstance: boolean, msg: FeatureList): FeatureList.AsObject;
  static serializeBinaryToWriter(message: FeatureList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FeatureList;
  static deserializeBinaryFromReader(message: FeatureList, reader: jspb.BinaryReader): FeatureList;
}

export namespace FeatureList {
  export type AsObject = {
    featuresList: Array<Feature.AsObject>,
  }
}

export class FeatureRequestId extends jspb.Message {
  getId(): string;
  setId(value: string): FeatureRequestId;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FeatureRequestId.AsObject;
  static toObject(includeInstance: boolean, msg: FeatureRequestId): FeatureRequestId.AsObject;
  static serializeBinaryToWriter(message: FeatureRequestId, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FeatureRequestId;
  static deserializeBinaryFromReader(message: FeatureRequestId, reader: jspb.BinaryReader): FeatureRequestId;
}

export namespace FeatureRequestId {
  export type AsObject = {
    id: string,
  }
}

export class FeatureResponse extends jspb.Message {
  getSuccess(): boolean;
  setSuccess(value: boolean): FeatureResponse;

  getStatus(): number;
  setStatus(value: number): FeatureResponse;

  getMessage(): string;
  setMessage(value: string): FeatureResponse;

  getData(): FeatureResponseData | undefined;
  setData(value?: FeatureResponseData): FeatureResponse;
  hasData(): boolean;
  clearData(): FeatureResponse;

  getStatusCase(): FeatureResponse.StatusCase;

  getMessageCase(): FeatureResponse.MessageCase;

  getDataCase(): FeatureResponse.DataCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FeatureResponse.AsObject;
  static toObject(includeInstance: boolean, msg: FeatureResponse): FeatureResponse.AsObject;
  static serializeBinaryToWriter(message: FeatureResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FeatureResponse;
  static deserializeBinaryFromReader(message: FeatureResponse, reader: jspb.BinaryReader): FeatureResponse;
}

export namespace FeatureResponse {
  export type AsObject = {
    success: boolean,
    status: number,
    message: string,
    data?: FeatureResponseData.AsObject,
  }

  export enum StatusCase { 
    _STATUS_NOT_SET = 0,
    STATUS = 2,
  }

  export enum MessageCase { 
    _MESSAGE_NOT_SET = 0,
    MESSAGE = 3,
  }

  export enum DataCase { 
    _DATA_NOT_SET = 0,
    DATA = 4,
  }
}

export class FeatureResponseData extends jspb.Message {
  getFeature(): Feature | undefined;
  setFeature(value?: Feature): FeatureResponseData;
  hasFeature(): boolean;
  clearFeature(): FeatureResponseData;

  getFeatureCase(): FeatureResponseData.FeatureCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FeatureResponseData.AsObject;
  static toObject(includeInstance: boolean, msg: FeatureResponseData): FeatureResponseData.AsObject;
  static serializeBinaryToWriter(message: FeatureResponseData, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FeatureResponseData;
  static deserializeBinaryFromReader(message: FeatureResponseData, reader: jspb.BinaryReader): FeatureResponseData;
}

export namespace FeatureResponseData {
  export type AsObject = {
    feature?: Feature.AsObject,
  }

  export enum FeatureCase { 
    _FEATURE_NOT_SET = 0,
    FEATURE = 1,
  }
}

export class FeaturesResponse extends jspb.Message {
  getSuccess(): boolean;
  setSuccess(value: boolean): FeaturesResponse;

  getStatus(): number;
  setStatus(value: number): FeaturesResponse;

  getMessage(): string;
  setMessage(value: string): FeaturesResponse;

  getData(): FeaturesResponseData | undefined;
  setData(value?: FeaturesResponseData): FeaturesResponse;
  hasData(): boolean;
  clearData(): FeaturesResponse;

  getStatusCase(): FeaturesResponse.StatusCase;

  getMessageCase(): FeaturesResponse.MessageCase;

  getDataCase(): FeaturesResponse.DataCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FeaturesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: FeaturesResponse): FeaturesResponse.AsObject;
  static serializeBinaryToWriter(message: FeaturesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FeaturesResponse;
  static deserializeBinaryFromReader(message: FeaturesResponse, reader: jspb.BinaryReader): FeaturesResponse;
}

export namespace FeaturesResponse {
  export type AsObject = {
    success: boolean,
    status: number,
    message: string,
    data?: FeaturesResponseData.AsObject,
  }

  export enum StatusCase { 
    _STATUS_NOT_SET = 0,
    STATUS = 2,
  }

  export enum MessageCase { 
    _MESSAGE_NOT_SET = 0,
    MESSAGE = 3,
  }

  export enum DataCase { 
    _DATA_NOT_SET = 0,
    DATA = 4,
  }
}

export class FeaturesResponseData extends jspb.Message {
  getFeaturesList(): Array<Feature>;
  setFeaturesList(value: Array<Feature>): FeaturesResponseData;
  clearFeaturesList(): FeaturesResponseData;
  addFeatures(value?: Feature, index?: number): Feature;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FeaturesResponseData.AsObject;
  static toObject(includeInstance: boolean, msg: FeaturesResponseData): FeaturesResponseData.AsObject;
  static serializeBinaryToWriter(message: FeaturesResponseData, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FeaturesResponseData;
  static deserializeBinaryFromReader(message: FeaturesResponseData, reader: jspb.BinaryReader): FeaturesResponseData;
}

export namespace FeaturesResponseData {
  export type AsObject = {
    featuresList: Array<Feature.AsObject>,
  }
}

