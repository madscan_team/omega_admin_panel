import { PlusOutlined } from '@ant-design/icons';
import { Button, message, Input, Drawer } from 'antd';
import React, { useState, useRef, useEffect } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { ModalForm, ProFormText, ProFormTextArea, DrawerForm, ProFormSwitch } from '@ant-design/pro-form';
import type { ProDescriptionsItemProps } from '@ant-design/pro-descriptions';
import ProDescriptions from '@ant-design/pro-descriptions';
import type { FormValueType } from './components/UpdateForm';
import UpdateForm from './components/UpdateForm';
import type { TableListItem } from './data.d';
import { queryRule, updateRule, addRule, removeRule } from './service';
import { getFeatures, createFeature, updateFeature, deleteFeature } from "../../api/methods";
/**
 * 添加节点
 * @param fields
 */
const handleAdd = async (fields: TableListItem) => {
  const hide = message.loading('正在添加');
  try {
    await addRule({ ...fields });
    hide();
    message.success('添加成功');
    return true;
  } catch (error) {
    hide();
    message.error('添加失败请重试！');
    return false;
  }
};

/**
 * 更新节点
 * @param fields
 */
const handleUpdate = async (fields: TableListItem) => {
  const hide = message.loading('正在配置');
  try {
    // await updateRule({ ...fields });
    hide();

    message.success('配置成功');
    return true;
  } catch (error) {
    hide();
    message.error('配置失败请重试！');
    return false;
  }
};

/**
 *  删除节点
 * @param selectedRows
 */
const handleRemove = async (selectedRows: TableListItem[], currentKey: any = null) => {
  const hide = message.loading('正在删除');
  // if (!selectedRows) return true;
  try {
    await removeRule({
      // key: selectedRows.map((row) => row.key),
      key: currentKey
    });
    hide();
    message.success('删除成功，即将刷新');
    return true;
  } catch (error) {
    hide();
    message.error('删除失败，请重试');
    return false;
  }
};

const ManageFeatures: React.FC = () => {
  /**
   * 新建窗口的弹窗
   */
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  /**
   * 分布更新窗口的弹窗
   */
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);

  const [showDetail, setShowDetail] = useState<boolean>(false);

  const actionRef = useRef<ActionType>();
  // const [currentRow, setCurrentRow] = useState<TableListItem>();
  const [selectedRowsState, setSelectedRows] = useState<TableListItem[]>([]);
  const [features, setFeatures]: any = useState([]);
  const [loader, setLoader] = useState(false);

  const [editStatus, setEditStatus] = useState(false);
  const [currentFeature, setCurrentFeature]: any = useState(null);
  /**
   * 国际化配置
   */
  const intl = useIntl();
  const columns: ProColumns<TableListItem>[] = [
    {
      title: <FormattedMessage id="pages.searchTable.active" defaultMessage="描述" />,
      dataIndex: 'active',
      valueType: 'code',
      render: (dom: any, entity: any) => <span style={{
        height: 10,
        width: 10,
        backgroundColor: JSON.parse(entity.defaultvalue?.toLowerCase()) ? '#22bb33' : '#bb2124',
        borderRadius: '50%',
        display: 'inline-block'
      }}></span>
    },
    {
      title: <FormattedMessage id="pages.searchTable.name" defaultMessage="描述" />,
      dataIndex: 'key',
      render: (dom, entity) => {
        return (
          <a
            onClick={() => {
              setCurrentFeature({
                defaultvalue: entity.defaultvalue,
                key: entity.key,
                id: entity.id,
                limit: entity.limit,
                valuetype: entity.valuetype
              });
              setEditStatus(true);
              // setCurrentRow(entity);
              // setShowDetail(true);
            }}
          >
            {dom}
          </a>
        );
      },
    },
    {
      title: <FormattedMessage id="pages.searchTable.limit" defaultMessage="描述" />,
      dataIndex: 'limit',
      valueType: 'digit',
    },
    {
      title: <FormattedMessage id="pages.searchTable.delete" defaultMessage="描述" />,
      dataIndex: 'delete',
      valueType: 'radioButton',
      render: (dom, entity) => {
        return (
          <Button type="ghost" onClick={async () => {
            await deleteFeature(entity.id);
            await handleRemove(selectedRowsState, entity.key);
            setSelectedRows([]);
            await getAllFeaturesMethod();
            actionRef.current?.reloadAndRest?.();
          }}>
            <FormattedMessage id="pages.searchTable.delete" defaultMessage="批量审批" />
          </Button>
        );
      },
    }
  ];

  const getAllFeaturesMethod = async () => {
    setLoader(true);
    const featuresRes: any = await getFeatures();
    setFeatures(featuresRes);
    setLoader(false);
  }

  useEffect(() => {
    getAllFeaturesMethod();
  }, [])

  return (
    <PageContainer>
      {!loader && <ProTable<TableListItem>
        headerTitle={intl.formatMessage({
          id: 'pages.searchTable.title',
          defaultMessage: '查询表格',
        })}
        actionRef={actionRef}
        rowKey="key"
        search={{
          labelWidth: 120,
        }}
        toolBarRender={() => [
          <Button
            type="primary"
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <PlusOutlined /> <FormattedMessage id="pages.searchTable.new" defaultMessage="新建" />
          </Button>,
        ]}
        request={(params, sorter, filter) => {
          // queryRule({ ...params, sorter, filter, features: JSON.stringify(features) })
          return Promise.resolve({
            data: features.reverse(),
            success: true,
          });
        }}
        columns={columns}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
        }}
      />}
      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.searchTable.chosen" defaultMessage="已选择" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.searchTable.item" defaultMessage="项" />
              &nbsp;&nbsp;
              <span>
                <FormattedMessage
                  id="pages.searchTable.totalServiceCalls"
                  defaultMessage="服务调用次数总计"
                />{' '}
                {/* {selectedRowsState.reduce((pre, item) => pre + item.callNo, 0)}{' '} */}
                <FormattedMessage id="pages.searchTable.tenThousand" defaultMessage="万" />
              </span>
            </div>
          }
        >
          <Button
            onClick={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
          >
            <FormattedMessage id="pages.searchTable.batchDeletion" defaultMessage="批量删除" />
          </Button>
          <Button type="primary">
            <FormattedMessage id="pages.searchTable.batchApproval" defaultMessage="批量审批" />
          </Button>
        </FooterToolbar>
      )}
      <ModalForm
        title={intl.formatMessage({
          id: 'pages.searchTable.createUser',
          defaultMessage: '新建规则',
        })}
        width="400px"
        visible={createModalVisible}
        onVisibleChange={handleModalVisible}
        onFinish={async (value) => {
          await createFeature({ ...value, defaultvalue: value.defaultvalue.toString().toUpperCase() });
          const success = await handleAdd(value as TableListItem);
          if (success) {
            handleModalVisible(false);
            if (actionRef.current) {
              await getAllFeaturesMethod();
              actionRef.current.reload();
            }
          }
        }}
      >
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.key"
                  defaultMessage="规则名称为必填项"
                />
              ),
            },
          ]}
          width="md"
          name="key"
          label="Key"
        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.limit"
                  defaultMessage="规则名称为必填项"
                />
              ),
            },
          ]}
          width="md"
          name="limit"
          label="Limit"
        />
        <ProFormSwitch
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.active"
                  defaultMessage="规则名称为必填项"
                />
              ),
            },
          ]}
          name="defaultvalue"
          label="Active"
        />
      </ModalForm>

      {editStatus && <DrawerForm
        width={600}
        visible={true}
        initialValues={currentFeature}
        title={intl.formatMessage({
          id: 'pages.searchTable.editFeature',
          defaultMessage: '新建规则',
        })}
        submitter={{
          render: (props, defaultDoms) => {
            return [
              <Button
                key="update"
                type="primary"
                onClick={() => props.submit()}
              >
                Update
              </Button>,
            ];
          },
        }}
        onVisibleChange={visible => {
          if (!visible) {
            setCurrentFeature(null);
            setEditStatus(false);
          }
        }}
        onFinish={async (value) => {
          await updateFeature({ ...value, defaultvalue: value.defaultvalue.toString().toUpperCase(), id: currentFeature.id });
          const success = await handleUpdate(value as TableListItem);
          if (success) {
            setEditStatus(false);
            if (actionRef.current) {
              await getAllFeaturesMethod();
              actionRef.current.reload();
            }
          }
        }}
      >
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.key"
                  defaultMessage="规则名称为必填项"
                />
              ),
            },
          ]}
          width="md"
          name="key"
          label="Key"
        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.limit"
                  defaultMessage="规则名称为必填项"
                />
              ),
            },
          ]}
          width="md"
          name="limit"
          label="Limit"
        />
        <ProFormSwitch
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.active"
                  defaultMessage="规则名称为必填项"
                />
              ),
            },
          ]}
          name="defaultvalue"
          label="Active"
        />
      </DrawerForm>}
    </PageContainer>
  );
};

export default ManageFeatures;
