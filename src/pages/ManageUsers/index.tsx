import { PlusOutlined } from '@ant-design/icons';
import { Button, message, Input, Drawer } from 'antd';
import React, { useState, useRef, useEffect } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { ModalForm, ProFormText, ProFormTextArea, DrawerForm } from '@ant-design/pro-form';
import type { ProDescriptionsItemProps } from '@ant-design/pro-descriptions';
import ProDescriptions from '@ant-design/pro-descriptions';
import type { FormValueType } from './components/UpdateForm';
import UpdateForm from './components/UpdateForm';
import type { TableListItem } from './data.d';
import { queryRule, updateRule, addRule, removeRule } from './service';
import { getUsers, createUser, deleteUser, updateUser } from "../../api/methods";
import Avatar from "../../assets/default-avatar.png";
import PropertyGrid from 'react-property-grid'
/**
 * 添加节点
 * @param fields
 */
const handleAdd = async (fields: TableListItem) => {
  const hide = message.loading('正在添加');
  try {
    await addRule({ ...fields });
    hide();
    message.success('添加成功');
    return true;
  } catch (error) {
    hide();
    message.error('添加失败请重试！');
    return false;
  }
};

/**
 * 更新节点
 * @param fields
 */
const handleUpdate = async (fields: TableListItem) => {
  const hide = message.loading('正在配置');
  try {
    await updateRule({ ...fields });
    hide();

    message.success('配置成功');
    return true;
  } catch (error) {
    hide();
    message.error('配置失败请重试！');
    return false;
  }
};

/**
 *  删除节点
 * @param selectedRows
 */
const handleRemove = async (selectedRows: TableListItem[], currentKey: any = null) => {
  const hide = message.loading('正在删除');
  // if (!selectedRows) return true;
  try {
    await removeRule({
      // key: selectedRows.map((row) => row.key),
      key: currentKey
    });
    hide();
    message.success('删除成功，即将刷新');
    return true;
  } catch (error) {
    hide();
    message.error('删除失败，请重试');
    return false;
  }
};

const ManageUsers: React.FC = () => {
  /**
   * 新建窗口的弹窗
   */
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  /**
   * 分布更新窗口的弹窗
   */
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);

  const [showDetail, setShowDetail] = useState<boolean>(false);

  const actionRef = useRef<ActionType>();
  const [currentRow, setCurrentRow] = useState<TableListItem>();
  const [selectedRowsState, setSelectedRows] = useState<TableListItem[]>([]);
  const [users, setUsers]: any = useState([]);
  const [loader, setLoader] = useState(false);

  const [editStatus, setEditStatus] = useState(false);
  const [currentUser, setCurrentUser]: any = useState(null);

  const [gridSchema, setGridSchema]: any = useState({});
  const [gridData, setGridData]: any = useState({});

  const [gridUpdatedData, setGridUpdatedData]: any = useState({});

  /**
   * 国际化配置
   */

  const generateSchemaAndData = (appList: any, appData: any) => {
    let schema = {
      type: "object",
      properties: {}
    };
    let data = {};
    appList.forEach((title: any) => {
      schema.properties[title] = {
        type: "object",
        properties: {}
      };
      data[title] = {};
      appData.forEach((arr: any) => {
        if (arr[0].split(':')[0] === title) {
          schema.properties[title].properties[arr[0].split(':')[1]] = {
            type: "object",
            properties: {
              [arr[0].split(':')[2] || "Nil"]: {
                type: "string"
              }
            }
          };
          data[title][arr[0].split(':')[1]] = {
            [arr[0].split(':')[2] || "Nil"]: arr[1],
            key: arr[0]
          };
        }
      })
    });
    setGridSchema(schema);
    setGridData(data);
  }

  const intl = useIntl();
  const columns: ProColumns<TableListItem>[] = [
    {
      title: <FormattedMessage id="pages.searchTable.avatar" defaultMessage="描述" />,
      dataIndex: 'avatar',
      valueType: 'avatar',
      render: (dom: any) => <img width="70" src={dom?.props?.text || Avatar} />
    },
    // {
    //   title: (
    //     <FormattedMessage
    //       id="pages.searchTable.updateForm.ruleName.nameLabel"
    //       defaultMessage="规则名称"
    //     />
    //   ),
    //   dataIndex: 'name',
    //   tip: '规则名称是唯一的 key',
    //   render: (dom, entity) => {
    //     return (
    //       <a
    //         onClick={() => {
    //           setCurrentRow(entity);
    //           setShowDetail(true);
    //         }}
    //       >
    //         {dom}
    //       </a>
    //     );
    //   },
    // },
    {
      title: <FormattedMessage id="pages.searchTable.name" defaultMessage="描述" />,
      dataIndex: 'name',
      render: (dom, entity: any) => {
        return (
          <a
            onClick={() => {
              generateSchemaAndData(entity.applicationsList, entity.applicationfeaturepermissionsMap);
              setCurrentRow(entity);
              setShowDetail(true);
            }}
          >
            {dom}
          </a>
        );
      },
    },
    {
      title: <FormattedMessage id="pages.searchTable.username" defaultMessage="描述" />,
      dataIndex: 'username',
      valueType: 'textarea',
    },
    {
      title: <FormattedMessage id="pages.searchTable._id" defaultMessage="描述" />,
      dataIndex: '_id',
      valueType: 'textarea',
    },
    {
      title: <FormattedMessage id="pages.searchTable.ipAddress" defaultMessage="描述" />,
      dataIndex: 'ipAddress',
      valueType: 'textarea',
    },
    {
      title: <FormattedMessage id="pages.searchTable.about" defaultMessage="描述" />,
      dataIndex: 'about',
      valueType: 'textarea',
      className: 'wordBreak'
    },
    {
      title: <FormattedMessage id="pages.searchTable.signature" defaultMessage="描述" />,
      dataIndex: 'signature',
      valueType: 'textarea',
    },
    {
      title: <FormattedMessage id="pages.searchTable.email" defaultMessage="描述" />,
      dataIndex: 'email',
      valueType: 'textarea',
    },
    {
      title: <FormattedMessage id="pages.searchTable.phoneNumber" defaultMessage="描述" />,
      dataIndex: 'phoneNumber',
      valueType: 'textarea',
    },
    {
      title: <FormattedMessage id="pages.searchTable.edit" defaultMessage="描述" />,
      dataIndex: 'edit',
      valueType: 'radioButton',
      render: (dom, entity: any) => {
        return (
          <Button type="primary" onClick={_ => {
            generateSchemaAndData(entity.applicationsList, entity.applicationfeaturepermissionsMap);
            setCurrentUser({
              about: entity.about,
              ipAddress: entity.ipAddress,
              key: entity.key,
              // firstName: entity.name.split(" ")[0] || "",
              // lastName: entity.name.split(" ")[1] || "",
              name: entity.name,
              phoneNumber: entity.phoneNumber,
              signature: entity.signature,
              _id: entity._id
            });
            setEditStatus(true);
            // handleModalVisible(true);
          }}>
            <FormattedMessage id="pages.searchTable.edit" defaultMessage="批量审批" />
          </Button>
        );
      },
    },
    {
      title: <FormattedMessage id="pages.searchTable.delete" defaultMessage="描述" />,
      dataIndex: 'delete',
      valueType: 'radioButton',
      render: (dom, entity) => {
        return (
          <Button type="ghost" onClick={async () => {
            await deleteUser(entity._id);
            await handleRemove(selectedRowsState, entity.key);
            setSelectedRows([]);
            await getAllUsersMethod();
            actionRef.current?.reloadAndRest?.();
          }}>
            <FormattedMessage id="pages.searchTable.delete" defaultMessage="批量审批" />
          </Button>
        );
      },
    }
    // {
    //   title: <FormattedMessage id="pages.searchTable.titleCallNo" defaultMessage="服务调用次数" />,
    //   dataIndex: 'callNo',
    //   sorter: true,
    //   hideInForm: true,
    //   renderText: (val: string) =>
    //     `${val}${intl.formatMessage({
    //       id: 'pages.searchTable.tenThousand',
    //       defaultMessage: ' 万 ',
    //     })}`,
    // },
    // {
    //   title: <FormattedMessage id="pages.searchTable.titleStatus" defaultMessage="状态" />,
    //   dataIndex: 'status',
    //   hideInForm: true,
    //   valueEnum: {
    //     0: {
    //       text: (
    //         <FormattedMessage id="pages.searchTable.nameStatus.default" defaultMessage="关闭" />
    //       ),
    //       status: 'Default',
    //     },
    //     1: {
    //       text: (
    //         <FormattedMessage id="pages.searchTable.nameStatus.running" defaultMessage="运行中" />
    //       ),
    //       status: 'Processing',
    //     },
    //     2: {
    //       text: (
    //         <FormattedMessage id="pages.searchTable.nameStatus.online" defaultMessage="已上线" />
    //       ),
    //       status: 'Success',
    //     },
    //     3: {
    //       text: (
    //         <FormattedMessage id="pages.searchTable.nameStatus.abnormal" defaultMessage="异常" />
    //       ),
    //       status: 'Error',
    //     },
    //   },
    // },
    // {
    //   title: (
    //     <FormattedMessage id="pages.searchTable.titleUpdatedAt" defaultMessage="上次调度时间" />
    //   ),
    //   sorter: true,
    //   dataIndex: 'updatedAt',
    //   valueType: 'dateTime',
    //   renderFormItem: (item, { defaultRender, ...rest }, form) => {
    //     const status = form.getFieldValue('status');
    //     if (`${status}` === '0') {
    //       return false;
    //     }
    //     if (`${status}` === '3') {
    //       return (
    //         <Input
    //           {...rest}
    //           placeholder={intl.formatMessage({
    //             id: 'pages.searchTable.exception',
    //             defaultMessage: '请输入异常原因！',
    //           })}
    //         />
    //       );
    //     }
    //     return defaultRender(item);
    //   },
    // },
    // {
    //   title: <FormattedMessage id="pages.searchTable.titleOption" defaultMessage="操作" />,
    //   dataIndex: 'option',
    //   valueType: 'option',
    //   render: (_, record) => [
    //     <a
    //       key="config"
    //       onClick={() => {
    //         handleUpdateModalVisible(true);
    //         setCurrentRow(record);
    //       }}
    //     >
    //       <FormattedMessage id="pages.searchTable.config" defaultMessage="配置" />
    //     </a>,
    //     <a key="subscribeAlert" href="https://procomponents.ant.design/">
    //       <FormattedMessage id="pages.searchTable.subscribeAlert" defaultMessage="订阅警报" />
    //     </a>,
    //   ],
    // },
  ];

  const getAllUsersMethod = async () => {
    setLoader(true);
    const usersRes = await getUsers(["_id", "username", "firstName", "lastName", "email", "profile", "phoneNumber", "signature", "about", "ipAddress"]);
    setUsers(usersRes);
    setLoader(false);
  }

  useEffect(() => {
    getAllUsersMethod();
  }, [])

  return (
    <PageContainer>
      {!loader && <ProTable<TableListItem>
        headerTitle={intl.formatMessage({
          id: 'pages.searchTable.title',
          defaultMessage: '查询表格',
        })}
        actionRef={actionRef}
        rowKey="key"
        search={{
          labelWidth: 120,
        }}
        toolBarRender={() => [
          <Button
            type="primary"
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <PlusOutlined /> <FormattedMessage id="pages.searchTable.new" defaultMessage="新建" />
          </Button>,
        ]}
        request={(params, sorter, filter) => {
          // queryRule({ ...params, sorter, filter, users: JSON.stringify(users) })
          return Promise.resolve({
            data: users.reverse(),
            success: true,
          });
        }}
        columns={columns}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
        }}
      />}
      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.searchTable.chosen" defaultMessage="已选择" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.searchTable.item" defaultMessage="项" />
              &nbsp;&nbsp;
              <span>
                <FormattedMessage
                  id="pages.searchTable.totalServiceCalls"
                  defaultMessage="服务调用次数总计"
                />{' '}
                {/* {selectedRowsState.reduce((pre, item) => pre + item.callNo, 0)}{' '} */}
                <FormattedMessage id="pages.searchTable.tenThousand" defaultMessage="万" />
              </span>
            </div>
          }
        >
          <Button
            onClick={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
          >
            <FormattedMessage id="pages.searchTable.batchDeletion" defaultMessage="批量删除" />
          </Button>
          <Button type="primary">
            <FormattedMessage id="pages.searchTable.batchApproval" defaultMessage="批量审批" />
          </Button>
        </FooterToolbar>
      )}
      <ModalForm
        title={intl.formatMessage({
          id: 'pages.searchTable.createUser',
          defaultMessage: '新建规则',
        })}
        width="400px"
        visible={createModalVisible}
        onVisibleChange={handleModalVisible}
        onFinish={async (value) => {
          await createUser(value);
          const success = await handleAdd(value as TableListItem);
          if (success) {
            handleModalVisible(false);
            if (actionRef.current) {
              await getAllUsersMethod();
              actionRef.current.reload();
            }
          }
        }}
      >
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.name"
                  defaultMessage="规则名称为必填项"
                />
              ),
            },
          ]}
          width="md"
          name="name"
          label="Name"
        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.ipAddress"
                  defaultMessage="规则名称为必填项"
                />
              ),
            },
          ]}
          width="md"
          name="ipAddress"
          label="IP Address"
        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.phoneNumber"
                  defaultMessage="规则名称为必填项"
                />
              ),
            },
          ]}
          width="md"
          name="phoneNumber"
          label="Phone Number"
        />
        <ProFormTextArea width="md" name="about" label="About" />
        <ProFormTextArea width="md" name="signature" label="Signature" />
      </ModalForm>

      {/* <UpdateForm
        onSubmit={async (value) => {
          const success = await handleUpdate(value);
          if (success) {
            handleUpdateModalVisible(false);
            setCurrentRow(undefined);
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
        onCancel={() => {
          handleUpdateModalVisible(false);
          setCurrentRow(undefined);
        }}
        updateModalVisible={updateModalVisible}
        values={currentRow || {}}
      /> */}

      <Drawer
        width={600}
        visible={showDetail}
        onClose={() => {
          setCurrentRow(undefined);
          setShowDetail(false);
        }}
        closable={false}
      >
        {currentRow?.name && (
          <ProDescriptions<TableListItem>
            column={2}
            title={currentRow?.name}
            request={async () => ({
              data: currentRow || {},
            })}
            params={{
              id: currentRow?.name,
            }}
            columns={columns.slice(0, columns.length - 2) as ProDescriptionsItemProps<TableListItem>[]}
          />
        )}
        <PropertyGrid schema={gridSchema} data={gridData} />
      </Drawer>

      {editStatus && <DrawerForm /* editStatus*/
        width={600}
        visible={true}
        initialValues={currentUser}
        title={intl.formatMessage({
          id: 'pages.searchTable.editUser',
          defaultMessage: '新建规则',
        })}
        submitter={{
          render: (props, defaultDoms) => {
            return [
              <Button
                key="update"
                type="primary"
                onClick={() => props.submit()}
              >
                Update
              </Button>,
            ];
          },
        }}
        onVisibleChange={visible => {
          if (!visible) {
            setCurrentUser(null);
            setEditStatus(false);
          }
        }}
        onFinish={async (value) => {
          setTimeout(async () => {
            await updateUser({ ...value, _id: currentUser._id, email: currentUser.email, permissions: gridUpdatedData });
            const success = await handleUpdate(value as TableListItem);
            if (success) {
              setEditStatus(false);
              if (actionRef.current) {
                await getAllUsersMethod();
                actionRef.current.reload();
              }
            }          
          }, 1000);
        }}
      >
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.name"
                  defaultMessage="规则名称为必填项"
                />
              ),
            },
          ]}
          width="md"
          name="name"
          label="Name"
        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.ipAddress"
                  defaultMessage="规则名称为必填项"
                />
              ),
            },
          ]}
          width="md"
          name="ipAddress"
          label="IP Address"
        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.phoneNumber"
                  defaultMessage="规则名称为必填项"
                />
              ),
            },
          ]}
          width="md"
          name="phoneNumber"
          label="Phone Number"
        />
        <ProFormTextArea width="md" name="about" label="About" />
        <ProFormTextArea width="md" name="signature" label="Signature" />

        <PropertyGrid onChange={(data: any) => setGridUpdatedData(data)} schema={gridSchema} data={gridData} />
      </DrawerForm>}
    </PageContainer>
  );
};

export default ManageUsers;
