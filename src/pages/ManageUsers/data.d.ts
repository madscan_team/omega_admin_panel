export type TableListItem = {
  key: number;
  disabled?: boolean;
  // href: string;
  avatar: string;
  name: string;
  username: string;
  ipAddress: string;
  _id: string;
  about?: string;
  signature?: string;
  phoneNumber?: string;
  email?: string;
  // owner: string;
  // desc: string;
  // callNo: number;
  // status: number;
  // updatedAt: Date;
  // createdAt: Date;
  // progress: number;

};

export type TableListPagination = {
  total: number;
  pageSize: number;
  current: number;
};

export type TableListData = {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
};

export type TableListParams = {
  status?: string;
  name?: string;
  desc?: string;
  key?: number;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
  users?: any;
  widgets?: any;
  features?: any
};
