import { application } from 'express';
import * as grpcWeb from 'grpc-web';
import { MainServiceClient } from '../grpc/MainServiceClientPb';
import {
    Empty,
    UsersResponse,
    UserRequestId,
    UserResponse,
    CreateUserRequest,
    UpdateUserRequest,
    RolesResponse,
    Role,
    RoleResponse,
    RoleRequestId,
    FeaturesResponse,
    Feature,
    FeatureRequestId,
    FeatureResponse
} from '../grpc/main_pb';

const mainService = new MainServiceClient('http://172.16.60.189:8080');

const BASE_URL = "http://172.16.60.108";

// let roleRequest2 = new Role();
// roleRequest2.setId(Math.random().toString());
// roleRequest2.setEntitlement('ent');
// let roleRequest3 = new RoleRequestId();
// roleRequest3.setId("603d1ed85f6d7f841c2d656c");


// mainService.deleteRoleById(roleRequest3, null, (err: grpcWeb.Error, response: RoleResponse) => {
//     if (response) {
//         console.log(response.toObject().data?.role, 'getrolebyid');
//     }
//     if (err) {
//         console.log(err, 'error1');
//     }
// });

// mainService.createRole(roleRequest2, null, (err: grpcWeb.Error, response: RoleResponse) => {
//     if (response) {
//         console.log(response.toObject().data?.role, 'createRole');
//     }
//     if (err) {
//         console.log(err, 'error1');
//     }
// });

export const getUsers = async (responseSchema: Array<string>) => {
    try {
        // const usersJSON = await fetch(`${BASE_URL}:3030/graphql`, {
        //     method: 'POST',
        //     headers: {
        //         'Content-Type': 'application/json',
        //         'Accept': 'application/json',
        //     },
        //     body: JSON.stringify({
        //         query: `{ 
        //             allUser { 
        //                 ${responseSchema.join()}
        //             } 
        //         }`
        //     })
        // });
        // const users = await usersJSON.json();
        // return users.data.allUser
        return new Promise((resolve, reject) => {
            let request = new Empty();
            const call1 = mainService.getAllUser(request, null,
                (err: grpcWeb.Error, response: UsersResponse) => {
                    if (response) {
                        console.log(response.toObject().data?.usersList, 'getAllUser');
                        let newSyntax = response.toObject().data?.usersList.map((user, index) => ({
                            ...user,
                            key: index,
                            avatar: user.profile,
                            name: `${user.firstname || ""} ${user.lastname || ""}`,
                            _id: user.id,
                            ipAddress: user.clientip,
                            username: user.username,
                            about: user.about,
                            signature: user.signature,
                            phoneNumber: user.phone,
                            email: user.email
                        }));
                        resolve(newSyntax);
                    }
                    if (err) {
                        reject(err);
                        console.log(err, 'error1');
                    }
                });
            call1.on('data', (data) => {
                console.log(data, 'getAllUserData')
            })
            call1.on('status', (status: grpcWeb.Status) => {
                console.log(status, 'getAllUserStatus')
            });
        })
    } catch (err) {
        console.log(err);
    }
}

export const createUser = async ({ name, ipAddress, phoneNumber, about, signature }: any) => {
    try {
        return new Promise((resolve, reject) => {
            let request = new CreateUserRequest();
            request.setId(Math.random().toString());
            request.setFirstname(name.split(" ")[0] || "");
            request.setLastname(name.split(" ")[1] || "Nil");
            request.setClientip(ipAddress || "");
            request.setPhone(phoneNumber || "");
            request.setAbout(about || "");
            request.setSignature(signature || "");
            request.setUsername("Nil");
            request.setEmail("Nil");
            request.setPassword("Nil");

            const call2 = mainService.createUser(request, null,
                (err: grpcWeb.Error, response: UserResponse) => {
                    if (response) {
                        console.log(response.toObject().data?.user, 'createuser');
                        resolve(response.toObject().data?.user);
                    }
                    if (err) {
                        reject(err);
                        console.log(err, 'error1');
                    }
                });
        })
    } catch (err) {
        console.log(err);
    }
}

export const deleteUser = async (id: any) => {
    try {
        return new Promise((resolve, reject) => {
            let request = new UserRequestId();
            request.setId(id);

            const call3 = mainService.deleteUserById(request, null,
                (err: grpcWeb.Error, response: UserResponse) => {
                    if (response) {
                        resolve(response.toObject().data?.user);
                        console.log(response.toObject().data?.user, 'deleteUser');
                    }
                    if (err) {
                        reject(err);
                        console.log(err, 'error1');
                    }
                });
        })
    } catch (err) {
        console.log(err);
    }
}

export const updateUser = async ({ name, ipAddress, phoneNumber, about, signature, _id, email, permissions }: any) => {
    try {
        return new Promise((resolve, reject) => {
            let applicationList: any = [];
            let mainArr = [];
            for(let ind in permissions) {
                mainArr.push(ind);
                for(let subInd in permissions[ind]) {
                    for(let key in permissions[ind][subInd]) {
                        if(key !== "key") {
                            applicationList.push([permissions[ind][subInd].key, permissions[ind][subInd][key]]);
                        }
                    }
                }
            }
            let request = new UpdateUserRequest();
            request.setId(_id);
            request.setFirstname(name.split(" ")[0]);
            request.setLastname(name.split(" ")[1] || "Nil");
            request.setClientip(ipAddress);
            request.setPhone(phoneNumber);
            request.setAbout(about || "");
            request.setSignature(signature || "");
            // request.setEmail(email);
            request.setApplicationsList(mainArr)
            request.getApplicationfeaturepermissionsMap()
            .set("MYAPP:marketDataPerm:Kavita", "TRUE")
            .set("MYAPP:marketDataPerm:Something", "TRUE");
            // return false
            // request.toObject().applicationfeaturepermissionsMap = applicationList

            const call4 = mainService.updateUserById(request, null,
                (err: grpcWeb.Error, response: UserResponse) => {
                    if (response) {
                        resolve(response.toObject().data?.user);
                        console.log(response.toObject().data?.user, 'updateUser');
                    }
                    if (err) {
                        reject(err);
                        console.log(err, 'error1');
                    }
                });
        })
    } catch (err) {
        console.log(err);
    }
}

export const getFeatures = async () => {
    try {
        return new Promise((resolve, reject) => {
            let request = new Empty();
            mainService.getAllFeature(request, null, (err: grpcWeb.Error, response: FeaturesResponse) => {
                if (response) {
                    console.log(response.toObject().data?.featuresList, 'getAllFeature');
                    resolve(response.toObject().data?.featuresList);
                }
                if (err) {
                    reject(err);
                    console.log(err, 'error1');
                }
            });
        })
    } catch (err) {
        console.log(err);
    }
}

export const createFeature = async ({ key, defaultvalue, limit }: any) => {
    try {
        return new Promise((resolve, reject) => {
            let request = new Feature();
            request.setId(Math.random().toString());
            request.setKey(key);
            request.setDefaultvalue(defaultvalue);
            request.setLimit(limit);
            request.setValuetype('bool');

            mainService.createFeature(request, null, (err: grpcWeb.Error, response: FeatureResponse) => {
                if (response) {
                    console.log(response.toObject().data?.feature, 'createFeature');
                    resolve(response.toObject().data?.feature);
                }
                if (err) {
                    console.log(err, 'error1');
                    reject(err);
                }
            });
        })
    } catch (err) {
        console.log(err);
    }
}

export const deleteFeature = async (id: any) => {
    try {
        return new Promise((resolve, reject) => {
            let request = new FeatureRequestId();
            request.setId(id);

            mainService.deleteFeatureById(request, null, (err: grpcWeb.Error, response: FeatureResponse) => {
                if (response) {
                    resolve(response.toObject().data?.feature);
                    console.log(response.toObject().data?.feature, 'deleteFeature');
                }
                if (err) {
                    reject(err);
                    console.log(err, 'error1');
                }
            });
        })
    } catch (err) {
        console.log(err);
    }
}

export const updateFeature = async ({ key, defaultvalue, limit, id }: any) => {
    try {
        return new Promise((resolve, reject) => {
            let request = new Feature();
            request.setId(id);
            request.setKey(key);
            request.setDefaultvalue(defaultvalue);
            request.setLimit(limit);
            request.setValuetype('bool');

            mainService.updateFeatureById(request, null, (err: grpcWeb.Error, response: FeatureResponse) => {
                if (response) {
                    console.log(response.toObject().data?.feature, 'updateFeature');
                    resolve(response.toObject().data?.feature);
                }
                if (err) {
                    reject(err);
                    console.log(err, 'error1');
                }
            });
        })
    } catch (err) {
        console.log(err);
    }
}

export const getWidgets = async (responseSchema: Array<String>) => {
    try {
        const widgetsJSON = await fetch(`${BASE_URL}:3030/graphql`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: JSON.stringify({
                query: `{ 
                    widgets { 
                        ${responseSchema.join()}
                    } 
                }`
            })
        });
        const widgets = await widgetsJSON.json();
        return widgets.data.widgets
    } catch (err) {
        console.log(err);
    }
}

export const updateWidget = async (data: any) => {
    try {
        await fetch(`${BASE_URL}:3030/graphql`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: JSON.stringify({
                query: `mutation UpdateWidget($data: UpdateWidget) {
                    updateWidget(widgetData: $data) {
                      success
                    }
                  }`,
                variables: { data }
            })
        });
    } catch (err) {
        console.log(err)
    }
}